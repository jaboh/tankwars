var acceleration = 0.56; // in pixel/ms^2
var maxSpeed = 7;//in pixels/s
// new speed = acceleration*dt +speed
//v(t) = at+b
//var gameTic = 10; //number of ms per tic

var turnDiameter = 0.006;//in pixels/s
//parameters to draw the tank
var halfLengthTank = 35;
var halfWidthTank = 21;
var distanceTank = Math.sqrt(halfLengthTank*halfLengthTank+halfWidthTank*halfWidthTank)
var radiusTank = 10;
var halfLengthBarrel = 37;
var halfWidthBarrel = 4;
var distanceBarrel = Math.sqrt(halfLengthBarrel*halfLengthBarrel+halfWidthBarrel*halfWidthBarrel)
var angle2 = Math.atan(halfWidthTank/halfLengthTank);
var angle1 =  3.14159265359 - angle2;
var angle3 = - angle2;
var angle4 = 3.14159265359 + angle2;
var angleB1 = 1.57079632679489661923;
var angleB2 = Math.atan(halfWidthBarrel/halfLengthBarrel);
var angleB3 = -angleB2;
var angleB4 = -1.57079632679489661923;

var canvas = document.querySelector('canvas');
canvas.width = 1000;
canvas.height = 800;
var c = canvas.getContext('2d');




var skipOnce = false;


function Tank (posX, posY, theta){
  this.x = posX;
  this.y = posY;
  this.speed = 0.0;
  this.angle = theta;
  this.passedTics = 0;
  this.findRect = function(x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, x6, y6){
    this.rectX1 = findMin(x1,x2,x3,x4,x5,x6);
    this.rectY1 = findMin(y1,y2,y3,y4,y5,y6);
    this.rectX2 = findMax(x1,x2,x3,x4,x5,x6);
    this.rectY2 = findMax(y1,y2,y3,y4,y5,y6);
  }
  this.setPoints = function(nextX,nextY){
    this.x1 = this.x + (Math.cos(this.angle+angle1) * distanceTank);
    this.y1 = this.y + (Math.sin(this.angle+angle1) * distanceTank);
    this.x2 = this.x + (Math.cos(this.angle+angle2) * distanceTank);
    this.y2 = this.y + (Math.sin(this.angle+angle2) * distanceTank);
    this.x3 = this.x + (Math.cos(this.angle+angle3) * distanceTank);
    this.y3 = this.y + (Math.sin(this.angle+angle3) * distanceTank);
    this.x4 = this.x + (Math.cos(this.angle+angle4) * distanceTank);
    this.y4 = this.y + (Math.sin(this.angle+angle4) * distanceTank);
    this.bx1 = this.x + (Math.cos(this.angle+angleB1) * halfWidthBarrel);
    this.by1 = this.y + (Math.sin(this.angle+angleB1) * halfWidthBarrel);
    this.bx2 = this.x + (Math.cos(this.angle+angleB2) * distanceBarrel);
    this.by2 = this.y + (Math.sin(this.angle+angleB2) * distanceBarrel);
    this.bx3 = this.x + (Math.cos(this.angle+angleB3) * distanceBarrel);
    this.by3 = this.y + (Math.sin(this.angle+angleB3) * distanceBarrel);
    this.bx4 = this.x + (Math.cos(this.angle+angleB4) * halfWidthBarrel);
    this.by4 = this.y + (Math.sin(this.angle+angleB4) * halfWidthBarrel);
  }
  this.drawTank = function(){
    if (this.forward){
      if(this.speed <= maxSpeed){
        this.speed += acceleration;
      }
      if(this.left){
        this.angle -= turnDiameter*this.speed;//watchout here
      }else if(this.right){
        this.angle += turnDiameter*this.speed;//watchout here
      }
    } else if(this.backward){
      if(this.speed >= -maxSpeed){
        this.speed -= acceleration;
      }
      if(this.left){
        this.angle -= turnDiameter*this.speed;//watchout here
      }else if(this.right){
        this.angle += turnDiameter*this.speed;//watchout here
      }
    } else {
      if(this.speed > 0){
        this.speed -= acceleration
        if(this.left){
          this.angle += turnDiameter*this.speed;//watchout here
        }else if(this.right){
          this.angle -= turnDiameter*this.speed;//watchout here
        }
      } else if(this.speed < 0){
        this.speed += acceleration
        if(this.left){
          this.angle -= turnDiameter*this.speed;//watchout here
        }else if(this.right){
          this.angle += turnDiameter*this.speed;//watchout here
        }
      }
      
    }
    /**
    * tank setup:
    *    (x1,y1)-------------------------------------(x2,y2)
    *      |                                            |
    *      |                 (bx1,by1)                  |         (bx2,by2)
    *      |                  ( )--------------------------------|
    *      |                  (_)--------------------------------|
    *      |                 (bx4,by4)                  |         (bx3.by3)
    *      |                                            |
    *    (x4,y4)-------------------------------------(x3,y3)
    *
    */
	
	setPoints(this.x + this.speed * Math.cos(this.angle), this.y + this.speed * Math.sin(this.angle));
	if(skipOnce){
	  this.findRect(this.x1,this.y1,this.x2,this.y2,this.bx2,this.by2,this.bx3,this.by3,this.x3,this.y3,this.x4,this.y4);
	  if( this.rectX1 < 0){
	    if(this.rectY1 < 0){
	      
		} else if(this.rectY2 > canvas.Height){
		  
		} else{
		  
		}
	  }else if(this.rectX2 > canvas.width){
	    if(this.rectY1 < 0){
	      
		} else if(this.rectY2 > canvas.Height){
		  
		} else{
		  
		}
	  }
	} else {
	  skipOnce = true;
	}
	c.beginPath();
	c.moveTo(this.x1,this.y1);
	c.lineTo(this.x2,this.y2);
	c.lineTo(this.x3,this.y3);
	c.lineTo(this.x4,this.y4);
	c.lineTo(this.x1,this.y1);
	c.stroke();
	c.fill();
	c.beginPath();
	c.moveTo(this.bx1,this.by1);
	c.lineTo(this.bx2,this.by2);
	c.lineTo(this.bx3,this.by3);
	c.lineTo(this.bx4,this.by4);
	c.stroke();
	c.fill();
	c.beginPath();
    c.arc(this.x, this.y, radiusTank,0,Math.PI * 2);
    c.stroke();
	c.fill();
    //quad(x1,y1,x2,y2,x3,y3,x4,y4);
    //quad(bx1,by1,bx2,by2,bx3,by3,bx4,by4);
    //ellipse(this.x,this.y,diameterTank);
  }
  this.fire = function(){
    fired = true;
  }
  this.forward = false;
  this.backward = false;
  this.left = false;
  this.right = false;
  this.fired = false;
}


var player1 = new Tank(100,100,0.0);
function setup(){
  //createCanvas(950,700);
  //fill(color(0,0,255));
  //frameRate(100);
  //var player1 = new Tank(100,100,0.0);
  //quad(10,10,10+lengthTank,10,10+lengthTank,10+widthTank,10,10+widthTank);
  //quad(45,31-widthBarrel/2, 45,31+widthBarrel/2,45+lengthBarrel,31+widthBarrel/2,45+lengthBarrel,31-widthBarrel/2);
  //ellipse(45, 31,diameterTank);
}

function draw(){
  //clear();
  //background(color(234,234,234));
  //player1.drawTank();
  //line(100,100,100+100*Math.cos(player1.angle),100+100*Math.sin(player1.angle));
}
var x = 0;

/*
function keyPressed(){
  if(keyCode == LEFT_ARROW){
    player1.left = true;
	console.log("left arrow pressed");
  } else if(keyCode == RIGHT_ARROW){
    player1.right = true;
  } else if(keyCode == UP_ARROW){
    player1.forward = true;
  } else if(keyCode == DOWN_ARROW){
    player1.backward = true;
  } else if(keyCode == 32){
    player1.fire();
  }
}

function keyReleased(){
  if(keyCode == LEFT_ARROW){
    player1.left = false;
  } else if(keyCode == RIGHT_ARROW){
    player1.right = false;
  } else if(keyCode == UP_ARROW){
    player1.forward = false;
  } else if(keyCode == DOWN_ARROW){
    player1.backward = false;
  }
}*/

document.addEventListener('keydown', (event) => {
  const keyName = event.key;
  if(event.key == "ArrowLeft"){
    player1.left = true;
  } else if(event.key == "ArrowRight"){
    player1.right = true;
  } else if(event.key == "ArrowUp"){
    player1.forward = true;
  } else if(event.key == "ArrowDown"){
    player1.backward = true;
  } else if(event.key == " "){
    player1.fire();
  }
}, false);

document.addEventListener('keyup', (event) => {
  const keyName = event.key;
  if(keyName == "ArrowLeft"){
    player1.left = false;
  } else if(keyName == "ArrowRight"){
    player1.right = false;
  } else if(keyName == "ArrowUp"){
    player1.forward = false;
  } else if(keyName == "ArrowDown"){
    player1.backward = false;
  }
}, false);


function findMax(value1, value2, value3, value4, value5, value6){
  let maxValue;
  if(value1 > value2){
    maxValue = value1;
  } else{
    maxValue = value2;
  }
  if(maxValue < value3){
    maxValue = value3;
  }
  if(maxValue < value4){
    maxValue = value4;
  }
  if(maxValue < value5){
    maxValue = value5;
  }
  if(maxValue < value6){
    maxValue = value6;
  }
  return maxValue;
}
function findMin(value1, value2, value3, value4, value5, value6){
  let minValue;
  if(value1 < value2){
    minValue = value1;
  } else{
    minValue = value2;
  }
  if(minValue > value3){
    minValue = value3;
  }
  if(minValue > value4){
    minValue = value4;
  }
  if(minValue > value5){
    minValue = value5;
  }
  if(minValue > value6){
    minValue = value6;
  }
  return minValue;
}


function animate(){
  c.fillStyle = "rgba(0,200,0,0.9)";
  c.rect(0,0,1000,800);
  c.fill();
  player1.drawTank();
  requestAnimationFrame(animate);
  
}
animate();
